Feature: 
  Test login funtionality

  #non parametertised
  #Scenario:
  #Given Browser is open
  #And user is on login page
  #When user enters username and password
  #And user clicks on login button
  #When user enters the remaining details
  #Then user will be taken to home page
  
  #parameterized
  Scenario Outline: 
    Given Browser is open
    And user is on login page
    When user enters <username> and <password>
    And user clicks on login button
    When user enters the remaining details
    Then user will be taken to home page

    Examples: 
      | username  | password |
      | srivatsa  |    12345 |
      | vanishree |    12345 |
