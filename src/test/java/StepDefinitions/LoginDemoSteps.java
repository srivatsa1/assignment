package StepDefinitions;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.*;


public class LoginDemoSteps {
	
	protected WebDriver driver=null;
	
	@Given("Browser is open")
	public void browser_is_open() {
		System.out.println("browser is open");
		System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\Skulcrushr\\\\Desktop\\\\hexaware\\\\Selenium\\\\chromedriver.exe");
		
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	@And("user is on login page")
	public void user_is_on_login_page() {
		driver.get("https://example.testproject.io/web/");
		
	
	}

	@When("^user enters (.*) and (.*)$")
	public void user_enters_username_and_password(String username,String password) throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(password);
		Thread.sleep(2000);
		
	}
	
	@And("user clicks on login button")
	public void user_clicks_on_login_button() {
		driver.findElement(By.xpath("//button[@id='login']")).click();
	
	}
	
	@When("user enters the remaining details")
	public void user_enters_the_remaining_details() throws InterruptedException {
		new Select(driver.findElement(By.xpath("//select[@id='country']"))).selectByVisibleText("India");
		driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/form[1]/div[2]/div[1]/input[1]")).sendKeys("kadur");
		driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/form[1]/div[3]/div[1]/input[1]")).sendKeys("srivatsa@gmail.com");
		driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/form[1]/div[4]/div[1]/input[1]")).sendKeys("9846135168");
		
	}

	

	@Then("user will be taken to home page")
	public void user_will_be_taken_to_home_page() throws InterruptedException {
		Thread.sleep(2000);
		
		
		String nameString=driver.findElement(By.xpath("//body/div[1]/div[1]/h1[1]")).getText();
		System.out.println("successfull "+nameString);
		
		driver.close();
		driver.quit();
		
	}


}
