
@smokeScenario
Feature: 
  feature to test login funtionality
	
	@smokeTest
  Scenario: 
    check login is successfull

    #steps:
    Given user is on login page
    When user enters username and password
    And user click on login button
    Then user is taken to home page

  #Scenario Outline: 
    #check login is successfull using parameters
#
    #steps:
    #Given user is on login page
    #When user enters <username> and <password>
    #And user click on login button
    #Then user is taken to home page
#
    #Examples: 
      #| username  | password |
      #| srivatsa  |      123 |
      #| vanishree |      123 |
